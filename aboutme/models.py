from django.db import models

# Create your models here.
class Biodata(models.Model):
    name = models.CharField(max_length=255, default="MUHAMAD ISTIADY KARTADIBRATA")
    edu = models.CharField(max_length=255, default="Computer Science, University of Indonesia")
    work = models.CharField(max_length=255, default="Front End Developer at adaTeman")
    exp = models.CharField(max_length=255, default="Staff of Project Management RISTEK 2019")
    ach = models.CharField(max_length=255, default="Business IT Case Finalist IT TODAY 2018")
    resume_link = models.TextField(default="https://docs.google.com/document/d/1sbcdL1-qMUAcnRjOG8i-b37UG_QnlFddfOhTDAL5J0I/edit?usp=sharing")

    def __str__(self):
        return str("biodata")