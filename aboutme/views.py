from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
from .models import Biodata
from django.forms.models import model_to_dict
import json
from django.views.decorators.csrf import csrf_exempt

def index(request):
    biodatas = Biodata.objects.all()
  

    return render(request,"about.html",{"biodatas":biodatas})

@csrf_exempt
def update(request):
    response ={}
    if(request.method=="POST"):
        nama = request.POST.get("name")
        resume = request.POST.get("resume")
        edu = request.POST.get("edu")
        exp = request.POST.get("exp")
        work = request.POST.get("work")
        ach = request.POST.get("ach")
        if len(nama) != 0 :
            Biodata.objects.filter(pk="1").update(name = nama)
        if len(resume) !=0 :
            Biodata.objects.filter(pk="1").update(resume_link= resume)
        if len(edu) !=0 :
            Biodata.objects.filter(pk="1").update(edu = edu)
        
        if len(exp) !=0 :
             Biodata.objects.filter(pk="1").update(work = work)
        if len(work) !=0 :
            Biodata.objects.filter(pk="1").update(exp=exp)
        
        if len(ach) !=0:
            Biodata.objects.filter(pk="1").update(ach = ach)
        return JsonResponse(response)
    else:
	    return HttpResponse(json.dumps({'message': "There's something wrong. Try again."}),content_type="application/json")


